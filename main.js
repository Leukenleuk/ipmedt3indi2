window.onload = function(){
  const sphere = document.getElementsByClassName('js--sphere');
  const save = document.getElementsByClassName('js--save');
  const redo = document.getElementsByClassName('js--redo');
  const huidig = document.getElementById('js--huidig');
  const mengen = document.getElementsByClassName('js--mengen');

  let pixel = document.getElementsByClassName('js--pixel');
  let brush = "rgb(0, 0, 0)";
  let brushoud = "";
  let kopie = [];
  var audio = new Audio('geluid.mp3');


  for(let i = 0; i< pixel.length; i++){
    pixel[i].onmouseenter = (event) => {
      pixel[i].setAttribute('color', brush);
      //audio afspelen
      audio.play();
    }
  }

  for(let i = 0; i< sphere.length; i++){
    sphere[i].onmouseenter = (event) => {
      brushoud = brush;
      brush = sphere[i].getAttribute('color');
      huidig.setAttribute('color', brush);
    }
  }

  for(let i = 0; i< mengen.length; i++){
    mengen[i].onmouseenter = (event) => {
      rgbOud = brushoud.substring(4, brushoud.length-1).replace(/ /g, '').split(',');
      rgb = brush.substring(4, brush.length-1).replace(/ /g, '').split(',');
      nieuw = [Math.min(parseInt(rgb[0])+parseInt(rgbOud[0]), 255), Math.min(parseInt(rgb[1])+parseInt(rgbOud[1]), 255), Math.min(parseInt(rgb[2])+parseInt(rgbOud[2]), 255)];
      brush = "rgb(" + nieuw[0] + ', ' + nieuw[1] + ', ' + nieuw[2] + ")";
      huidig.setAttribute('color', brush);
    }
  }

  for(let i = 0; i<save.length; i++){
    save[i].onmouseenter = (event) => {
      save[i].setAttribute('color', 'green');//
      kopie =[];
      for (let j = 0; j < pixel.length; j++) {
        kopie.push(pixel[j].getAttribute('color'));
      }
    }
    save[i].onmouseleave = (event) => {
      save[i].setAttribute('color', 'rgb(255, 255, 255)');//
    }
  }

  for(let i = 0; i<redo.length; i++){
    redo[i].onmouseenter = (event) => {
      redo[i].setAttribute('color', 'blue');
        for (let j = 0; j < pixel.length; j++) {
          pixel[j].setAttribute('color', kopie[j]);
        }
      }

    redo[i].onmouseleave = (event) => {
      redo[i].setAttribute('color', 'rgb(255, 255, 255)');//
    }
  }
}
